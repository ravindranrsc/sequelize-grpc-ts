import dbModuls from "../models/index";

type ContryTab = { name: string };

const Contry = dbModuls.contry;
/* Contry.create({name:'India'});
Contry.create({name:'Singapore'});
Contry.create({name:'America'});
Contry.create({name:'Japan'});
Contry.create({name:'Austreliya'}); */

const addContry = async (inp: ContryTab) => {
  const ret = await Contry.create(inp);
};
const addMultipulContry = async (inp: ContryTab[]) => {
  inp.forEach(async (data) => {
    const ret = await Contry.create(data);
  });
};

/* module.exports = {
  addContry,
  addMultipulContry,
};
 */

export { addContry, addMultipulContry };
