// export {};
import dbModuls from "../models/index";

const State = dbModuls.state;
const Contry = dbModuls.contry;

const addState = (inp: any) => {
  State.create({ name: inp });
};

const addMultipulState = (inp: any[]) => {
  inp.forEach(async (data) => {
    const resv = State.create(data);
  });
};

const addStateAndContry = async (inp: any) => {
  const contry_name = inp.contry_name;
  const state_name = inp.state_name;
  /*   const contry = await Contry.create({ name: contry_name })
    .then((data: any) => {
      console.log("Country Created");
      console.log(data);
      data.createState({ name: state_name });
    })

    .catch((err: any) => {
      console.log(err);
    }); */

  const contry = await Contry.create({ name: contry_name });
  const state = await contry.createState({ name: state_name });

  // console.log(resstate);
};

const asignStateContry = async (inp: any) => {
  let contry = await Contry.findOne({ where: { id: inp.contry_id } });
  let state = await State.findOne({ where: { id: inp.state_id } });
  contry.setState(state);
};

const addStateUsingInstance = async (
  ContrtyName: string,
  StateName: string
) => {
  const state2 = await State.build({ name: StateName });
  state2.contryId = 1;
  console.log(state2);

  state2.save();
  console.log(state2.toJSON());
  console.log("**************************************************");
  console.log(JSON.stringify(state2, null, 4));
  console.log("**************************************************");
  console.log(JSON.stringify(state2));
};

/* module.exports = {
  addState,
  addMultipulState,
  addStateAndContry,
  asignStateContry,
  addStateUsingInstance,
}; */

export {
  addState,
  addMultipulState,
  addStateAndContry,
  asignStateContry,
  addStateUsingInstance,
};
