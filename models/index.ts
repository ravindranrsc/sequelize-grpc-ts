const dbConfig = require("../config/dbconfig");
const { Sequelize, DataTypes } = require("sequelize");

const db: any = {};

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorAliases: 0,
  pool: {
    min: dbConfig.min,
    max: dbConfig.max,
    acquire: dbConfig.acquire,
    idle: dbConfig.idle,
  },
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

sequelize
  .authenticate()
  .then(() => {
    console.log("Connected");
  })
  .catch((err: any) => {
    console.log("err");
  });

import { Contry } from "../models/contry";
import { State } from "../models/state";
import { Customer } from "../models/customer";

db.contry = Contry(sequelize, DataTypes);
db.state = State(sequelize, DataTypes);
db.customer = Customer(sequelize, DataTypes);
db.contry.hasMany(db.state);

db.customer.belongsTo(db.state, {
  foreignKey: "state_id",
  as: "state",
});
db.customer.belongsTo(db.contry, {
  foreginKey: "contry_id",
  as: "contry",
});

/* 
Property.hasMany(PropertyTranslation);
PropertyTranslation.belongsTo(Property);
Property.hasOne(PropertyAttribute);
Property.belongsTo(Project);
Property.belongsTo(Developer);
Property.hasMany(PropertyFile);
PropertyFile.belongsTo(Property);

Property.hasMany(PropertyConversation);
PropertyConversation.belongsTo(Property);
PropertyAttribute.belongsTo(Property);
Property.belongsTo(User);
Property.belongsTo(User, { as: 'Subuser', foreignKey: 'subuser_id' });
User.hasMany(Property);
Property.hasOne(PropertyLocation);
Property.hasOne(PropertyStatusLog);
PropertyLocation.belongsTo(Property);
Property.belongsTo(TypeMaster, {
    foreignKey: 'listing_type_id',
    as: 'listsType',
});
Property.belongsTo(TypeMaster, {
    foreignKey: 'status_type_id',
    as: 'propertyStatus',
});

Property.belongsTo(TypeMaster, {
    foreignKey: 'source_type_id',
    as: 'sourceType',
});
 */

sequelize
  .sync({ alter: true })
  .then((data: any) => {
    console.log("Sync Done");
  })
  .catch((err: any) => {
    console.log(err);
  });

// module.exports.db = db;
/* module.exports = {
  dbModuls: db,
};
 */

export default db;
