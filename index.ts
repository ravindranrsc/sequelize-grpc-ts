export {};
const {
  addContry,
  addMultipulContry,
} = require("./controllers/countryController");
const {
  addStateAndContry,
  asignStateContry,
  addStateUsingInstance,
} = require("./controllers/stateControllers");

const { addCutomer } = require("./controllers/customerController");

const { db } = require("./models/index");

const express = require("express");
const app = express();
app.use(express.json());
app.use(express.urlencoded({ exrended: false }));
const PORT = process.env.PORT || 7070;
app.listen(PORT, () => {
  console.log("started");
});

// add({name:"Malaysia"});
//addMultipul([{name:"Nepal"},{name:"SriLanka"}]);

// addMultipul([{ name: "German" }, { name: "France" }]);

// console.log(db);

/* import defaultFunction, { add, subtract } from './testexport';

console.log(add(1, 2)) // 3
console.log(subtract(2, 1)) // 1
console.log(defaultFunction(2, 2)) // 2 * 2 = 4

import * as MyFun from "./testexport";
console.log(MyFun.add(2,8));
console.log(MyFun.default(5,5)); */
//const stanteobj = { state_name: "Tamil Nadu", contry_id: 1 };

//addStateAndCountry(stanteobj);

// addContry({ name: "Barath" });

// addMultipulContry([
//   { name: "India" },
//   { name: "Singapore" },
//   { name: "America" },
//   { name: "Austreliya" },
//   { name: "Japan" },
// ]);

// addStateAndContry({ contry_name: "India", state_name: "Delhi" });
// addStateAndContry({ contry_name: "Singapore", state_name: "Singapore" });
// addStateAndContry({ contry_name: "America", state_name: "Washington" });
// addStateUsingInstance("India", "Karnataka");
addCutomer({
  name: "Ravindran",
  doorno: "1A",
  street: "Sivagami Nagar",
  place: "Veerakeralam",
  pin: "641007",
  state_id: 1,
  coluntry_id: 1,
});
addCutomer({
  name: "Revathi",
  doorno: "33",
  street: "AKR Road NO3",
  place: "Tiruchengode",
  pin: "637211",
  state_id: 1,
  coluntry_id: 1,
});

addCutomer({
  name: "Daksehs",
  doorno: "1A",
  street: "Sivagami Nagar",
  place: "Veerakeralam",
  pin: "641007",
  state_id: 1,
  coluntry_id: 1,
});

addCutomer({
  name: "Harshita",
  doorno: "1A",
  street: "Sivagami Nagar",
  place: "Veerakeralam",
  pin: "641007",
  state_id: 1,
  coluntry_id: 1,
});

addCutomer({
  name: "Aravinth.E",
  doorno: "30",
  street: "Sivagami Nagar",
  place: "Vadachitoor",
  pin: "6410028",
  state_id: 1,
  coluntry_id: 1,
});

//asignStateContry({ contry_id: 1, state_id: 1 });
