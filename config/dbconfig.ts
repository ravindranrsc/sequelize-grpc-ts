module.exports = {
  HOST: "localhost",
  USER: "postgres",
  PASSWORD: "asdfgf",
  DB: "share_trade",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 1000,
  },
};
